/*
 * Nikolai Lubiagov <nikolai.lubiagov@lurity.com>, FeverScreener modbus master protocol sample
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <termios.h>
#include <pthread.h>
#include <modbus/modbus-rtu.h>

void noCanonicalMode(){
    struct termios info;
    tcgetattr(0, &info);          /* get current terminal attirbutes; 0 is the file descriptor for stdin */
    info.c_lflag &= ~ICANON;      /* disable canonical mode */
    info.c_cc[VMIN] = 1;          /* wait until at least one keystroke available */
    info.c_cc[VTIME] = 0;         /* no timeout */
    tcsetattr(0, TCSANOW, &info); /* set immediately */
}

typedef struct protocolconf_t{
    modbus_mapping_t *mapping;
    modbus_t *ctx;
    int isrunned;
    pthread_mutex_t mutex;
    int startDetector;
    int detectMask;
    int detectTemperature;
    int signalMode;
    int showThermalImage;
    int showQuestion;
} protocolconf_t;

#define PERSON_LEFT 1
#define PERSON_HERE_REGULAR 2
#define PERSON_HERE_SIGNAL 3
#define PERSON_DETECT_TIMEOUT 4

//Mask statuse, if mask detection disabled, this will return `Can not detect mask`
char* mask_statuses[]={"Has not mask","Has mask","Wrong fit mask","Can not detect mask"};
char* boolean_stauses[]={"disabled","enabled"};
char* person_states[]={"","Person left","Person stay here, mask/temperature is ok (regular)",
                       "Person stay here,mask/temperature problem (signal)","face detection timeout...",
                       "Mask or temperature state updated"};
char* temperature_states[]={"WrongTemperature","NormalTemperature","IncreasedTemperature","HighTemperature","LoweredTemperature"};
char* question_status[]={"NotResponded","PositiveAnswer","NegativeAnswer"};

//device address in Modbus network
uint8_t slaveDeviceId=3;

//5, it is first register, for write data, and we fill 2 next:
//5 - start detection, once write 1, and master (device) will change it to 0, when face detector will started;
//6 - 1 for detect mask, 0 if we don't want detect mask;
//7 - 1 for detect temperature, 0 if we don't need temperature detector;
//8 - Time in second, how long device will detect face;
//9 - 0 if we fill in the data with the mask on and normal temperature, 1 otherwise (signal mode);
//10 - 1 if you want to display the image from the temperature sensor, on the screen in real time
uint8_t controlAddress=5;

//Result address, here master will write detection result:
//20 - 1 if face normal detected, and measure finished. 2 otherways.
//Before start detection (after read result) slave should set this register to 0;
//21,22 - save float variable, with measured temperature. 0 - if ew cannot measure temperature;
//23 - mask result, 0 - HasNotMask, 1 - HasMask, 2 - WrongFitMask, 3 - CanNotDetectMask;
//24 - temperature measurement status, 0 - wrong temperature, 1- normal temperature, 2- Increased temperature,
//     3 - high temperature, 4 - lowered remperature.
uint8_t resultAddress=20;

//TODO Time, in second, how long device will detect face. If this time expired device will
//return error status in resultAddress.
uint8_t detectingTimeout=20;

void* threadfunc (void * args){    
    protocolconf_t* modbusconf=(protocolconf_t*)args;
    while(1){
        pthread_mutex_lock(&(modbusconf->mutex));
        if(!modbusconf->isrunned){
            pthread_mutex_unlock(&(modbusconf->mutex));
            break;
        }
        uint16_t* regs=modbusconf->mapping->tab_registers;

        //When device will start detecting, it reset this register to 0
        if(regs[controlAddress]==1){
            printf("wait for start face detector on device...\n");
        }
        if(regs[controlAddress]==2){
            printf("wait for stop face detector on device...\n");
        }

        if(modbusconf->startDetector==1){
            modbusconf->startDetector=0;
            //device in loop read this registers, every 500 millisecond
            regs[controlAddress]=1;//request device start detector
            regs[controlAddress+1]=modbusconf->detectMask; //1 if we need detect mask
            regs[controlAddress+2]=modbusconf->detectTemperature; //1 if we need detect temperature
             //TODO tme, how long detector will work, while it found face and measure temperature
            regs[controlAddress+3]=detectingTimeout;
            regs[controlAddress+4]=modbusconf->signalMode;
            regs[controlAddress+5]=modbusconf->showThermalImage;
            regs[controlAddress+6]=modbusconf->showQuestion;
        }
        if(modbusconf->startDetector==2){
            modbusconf->startDetector=0;
            regs[controlAddress]=2;//request device start detector
        }
        //if we have result, this register will be not zero, 1- if success, TODO 2- ot other value in error case
        if(regs[resultAddress]){
            //set register to 0, for prevent loop. (TODO for send device, successful data reading)
            const int personStatus=regs[resultAddress];
            regs[resultAddress]=0;
            if(personStatus==PERSON_DETECT_TIMEOUT){
                printf("\ntimeout, can't detect face in camera area!\n");
            }else{
                //Get temperature as 32 bif variable, combined in 2 register. Temperature maybe 0, if it disabled,
                //or temperature measure problem, for example person leave camera area before detect temperature,
                //too hot object in camera or temperature service problems.
                float temperature=modbus_get_float(regs+resultAddress+1);
                int maskstatus=regs[resultAddress+3];//recive mask status
                int temperature_status=regs[resultAddress+4];
                int question_response=regs[resultAddress+5];
                //Print measure result, in console output
                printf("\nyour temperature: %f [%s], mask state: %s, state: %s question state: %s\n",temperature,
                       temperature_states[temperature_status], mask_statuses[maskstatus],
                       person_states[personStatus],question_status[question_response]);
            }
        }
        pthread_mutex_unlock(&(modbusconf->mutex));

        uint8_t req[MODBUS_RTU_MAX_ADU_LENGTH];// request buffer
        //TODO interupt modbus_receive in application exit request
        int len = modbus_receive(modbusconf->ctx, req);
        if (len == -1) {
            printf("modbus_receive error %s\n", modbus_strerror(errno));
            continue;
        }        
        len = modbus_reply(modbusconf->ctx, req, len, modbusconf->mapping);
        if (len == -1) {
            printf("modbus_reply error: %s\n", modbus_strerror(errno));
            continue;
        }
    }
    return NULL;
}

int main(){
    noCanonicalMode();
    protocolconf_t modbusconf;
    memset(&modbusconf,0x00,sizeof (protocolconf_t));
    modbusconf.isrunned=1;
    modbusconf.detectMask=1;
    modbusconf.detectTemperature=1;

    //TTY interface device, i use USB-RS485 adapted based on CH341a and MAX485 on PC side.
    //On device side we have we use TTY interface in RK3399 CPU, and MAX485, ~RE/DE pins
    //connected to GPIO interface. Unfortunally it have not automaticale RTS control,
    //becouse we do it manully.
    modbusconf.ctx = modbus_new_rtu("/dev/ttyUSB0", 19200, 'N', 8, 1);
    if (modbusconf.ctx == NULL) {
        fprintf(stderr, "Unable to create the libmodbus context\n");
        return -1;
    }

    if (modbus_set_slave(modbusconf.ctx, slaveDeviceId)){
        printf("modbus_set_slave error %d - %s\n",errno,strerror(errno));
        modbus_free(modbusconf.ctx);
        return -1;
    }

    if (modbus_connect(modbusconf.ctx) == -1) {
        printf("modbus_connect error %d - %s\n",errno,strerror(errno));
        modbus_free(modbusconf.ctx);
        return -1;
    }

    //Use only read/write registers, in current we use [5-8] registers for control device
    //and [10-13] for recive result from device.
    modbusconf.mapping = modbus_mapping_new(0, 0, 50, 0);
    modbus_set_error_recovery(modbusconf.ctx, MODBUS_ERROR_RECOVERY_PROTOCOL);

    pthread_t thread;
    pthread_mutex_init(&(modbusconf.mutex),NULL);
    //create thread, here will be infinity loop, for read and send data over modbus protocol
    if(pthread_create(&thread, NULL, threadfunc, &modbusconf)){
        printf("Start thread error %d - %s\n",errno,strerror(errno));
        modbus_close(modbusconf.ctx);
        modbus_free(modbusconf.ctx);
        modbus_mapping_free(modbusconf.mapping);
        return 0;
    }

    char selecteditem;
    do{
        printf("press 1 - start face detector;\n"
               "press 2 - stop face detector;\n"
               "press 3 - %s mask detector;\n"
               "press 4 - %s temperature detector;\n"
               "press 5 - %s signal mode\n"
               "press 6 - %s thermal image\n"
               "press 7 - %s show question dialog\n"
               "press q - for exit.\n"
               "Select option:",
               boolean_stauses[modbusconf.detectMask],
               boolean_stauses[modbusconf.detectTemperature],
               boolean_stauses[modbusconf.signalMode],
               boolean_stauses[modbusconf.showThermalImage],
               boolean_stauses[modbusconf.showQuestion]);
        selecteditem=getchar();
        printf("\n");
        pthread_mutex_lock(&(modbusconf.mutex));
        switch (selecteditem) {
            case '1':
                modbusconf.startDetector=1;
                break;
            case '2':
                modbusconf.startDetector=2;
                break;
            case '3':
                modbusconf.detectMask=!modbusconf.detectMask;
                break;
            case '4':
                modbusconf.detectTemperature=!modbusconf.detectTemperature;
                break;
            case '5':
                modbusconf.signalMode=!modbusconf.signalMode;
                break;
            case '6':
                modbusconf.showThermalImage=!modbusconf.showThermalImage;
                break;
            case '7':
                modbusconf.showQuestion=!modbusconf.showQuestion;
        }
        pthread_mutex_unlock(&(modbusconf.mutex));
    }while (selecteditem!='q');
    modbusconf.isrunned=0;
    pthread_join(thread,NULL);    
    pthread_mutex_destroy(&(modbusconf.mutex));
    modbus_close(modbusconf.ctx);
    modbus_free(modbusconf.ctx);
    modbus_mapping_free(modbusconf.mapping);
    return 0;
}
