## **FeverScreener modbus protocol sample**
This source code provides an example of interaction with the Fever Screener device using the Modbus-RTU protocol and RS485 interface.

---

## Requirements

* The code was tested in Ubuntu 18.04;
* GCC, tested in 7.5.0 version;
* libmodbus, tested in 3.10.2-1ubuntu2.18.04.1;
* CMake, tested in 3.16.1 version;
* git.

**To install the dependencies, execute:**

```
$ sudo apt-get install git gcc libmodbus-dev cmake
```

We also need an RS485 interface, on a PC. I used this adapter, based on the CH341a and MAX485.

![alt text](https://bitbucket.org/sentiner/modbusmastersample/raw/master/adapter.jpg)

The outputs `RS485-A` and` RS485-B` of the device connector must be connected to the corresponding outputs of the adapter, using twisted pair cable.

The protocol specification also assumes the installation of terminal resistors, with a resistance of 120Ω at the ends of the line, but I did not do this. Also, the GND pin on the device must be connected to the PC cover, or GND pin in power supply to create a common signal ground.

---

## Building from sources
To build the sources run the following commands:
```
$ git clone git@bitbucket.org:sentiner/modbusmastersample.git
$ mkdir build && cd build
$ cmake ..
$ make
```

---

## Run application
Since TTY interfaces do not provide access to non-privileged users, you can add your user to the `dialout` group, to avoid running as `root`:

```
$ sudo usermod -a -G dialout <your username>
```

Run application:

```
# ./test_modbus
```

---

## Transfer protocol description

The communication protocol with the device is based on the Modbus-RTU protocol. The Modbus protocol specification can be found on the official website https://modbus.org/specs.php. Both parts of the system, FeverScreener device and this example, use the libmodbus library to implement the exchange protocol.

In the exchange protocol, only Holding Registers are used, that are available for reading and writing (function codes for accessing the registers 0x03, 0x06, 0x10, 0x17).

Two blocks of registers are selected for interaction. One block consists of six registers, and is used to control the face detector on the device, hereinafter in this document we will designate it as the `control block`. The second block consists of five registers and is used to receive the result from the device, we designate it by the `result block`.

The starting addresses of the blocks can be specified in the device settings in the portal. Settings the RS485 port on device, baud rate, data word size, parity, and the number of stop bits, is also can be specified using the portal.

For the system to function, both options, “Modbus” and “RS485” must be enabled (Zapnuté) in the device settings.

To use this example, you must specify the following values:

![alt text](https://bitbucket.org/sentiner/modbusmastersample/raw/master/PortalConfig.png)

Addresses of register blocks must not overlap. The maximum number of the storage register, in accordance with the modbus protocol specification 121. Since the `control block` consist of six registers, the address value range must be between 1 and 115. The `result block` contains five registers, and its address must be in the range from 0 to 116, respectively.

The FeverScreener device is a Master device in the Modbus network, and accordingly the control device is a Slave device.

**Description of registers for device interaction**

In a cycle with an interval of 500 milliseconds, the Master device polls the Slave device by calling the 0x03 function (modbus_read_registers), reading the registers in the `control block`.

|offset|flag name|description|
|-|-|-|
|0|detector start flag | Slave sets the value to 1 to start the detector on the device (Master) or Slave can set the value to 2, to stop the face detector if it was started. After starting or stop the detector, the `master device` will call the function 0x06 (modbus_write_register) by writing the value 0, to the register indicating that the face detector has been started/stopped.|
|1|Mask flag| It must be set by the Slave device to 1, if it is necessary to turn on the mask detector.|
|2|Temperature flag| It must be set by the Slave device to 1, if necessary to measure temperature.|
|3|Time parameter| The time in seconds that determines the duration of the detector. If after a time the not obtained result is, the error status will be recorded in the `result block`.|
|4|Signal mode| Turn on the alarm mode. If true, the device will return a response if there is no mask or high temperature. If false, the device will return a response when the mask is correctly put on, and the temperature value is normal.|
|5|Thermal image| Turns on the display of the image obtained from the thermal sensor. And also the temperature in real time from this image. Thus, the temperature and the hottest spot can be seen in real time.|

The result registers block are recorded by calling the 0x10 functions (modbus_write_registers), by the Master device, recording the result of temperature measurement and mask detector operation.

Description of the registers in the `result block`:

|offset|flag name|description|
|-|-|-|
|0|Flag of successful measurement| A list of possible values is given below. After reading the Slave result, the device should set the register value to 0.|
|1,2|Temperature measurement result| It consists of two 16-bit registers, combined together for recording 32-bit floating-point value. To get the value, you can use modbus_get_float function, passing the address of the first register as a parameter. The temperature value may be 0, in case of errors. For example in case of problems with the temperature detector, if a person has gone out from the temperature sensor measurement area before closure, or the temperature detector has been disabled in `control block`.|
|3|Mask state result| Possible values are shown in the following table.|
|4|Temperature state result| Possible values are shown in the following table.|

To obtain the register address, it is necessary to add its offset to the value in the field `Face control address` or `Face result address` specified in the portal in the device settings, depending on the block type.

List of possible `flag of successful measurement` values:

|Register value|Constant name|Description|
|-|-|-|
|0|-|Initial state|
|1|PERSON_LEFT|The result is returned. Person left the camera area. The detector has been stopped after this response.|
|2|PERSON_HERE_REGULAR|It is returned if signal_mode = false. The person is in the area of the camera, while the mask and temperature are measured and are acceptable. Here you can open the turnstile.|
|3|PERSON_HERE_SIGNAL|It is returned if signal_mode = ture. The person is in the camera zone. It has either a high fever or an improperly worn mask. You can turn on the alarm.|
|4|PERSON_DETECT_TIMEOUT|The detector was started, however, the face was not found, during the response time in the `time parameter`. The detector has been stopped after this response.|
|5|PERSON_DETECT_UPDATED| Updated the state of the mask, or temperature. It is returned every time the temperature changes between `normal`,` elevated` or `high`, as well as if the person puts on or takes off the mask. The same is returned for the first calculation of the temperature. Immediately after this event, the device will play an audio message and display the new status on the screen.|

List of possible `mask state result` values:

|Register value|Constant in Firebase|Description|
|-|-|-|
|0|HasNotMask|The detector successfully detected the absence of a mask on a person’s face.|
|1|HasMask|The detector has successfully detected a correctly worn mask.|
|2|WrongFitMask|Mask is not worn correctly. For example , the nose is open, or the mouth is open.|
|3|CanNotDetectMask|Unable to determine mask presence. This value may indicate the error im the mask detector, for example, the neural network of the detector could not be loaded, or if the mask detector was disabled in the `control block`.|


List of possible `temperature state result` values:

|Register value|Constant in Firebase|Temperature measurement result|Description|
|-|-|-|-|
|0|WrongTemperature| 0 | This means that the temperature or could not be measured, because the person left the core before the end of the measurement. Or the temperature sensor does not work on the device.|
|1|NormalTemperature| <=37.0 |-|
|2|IncreasedTemperature| > 37.0 and < 38.0 |-|
|3|HighTemperature| in other cases (>=38.0) |-|
|4|LoweredTemperature| <36.0 |-|

## Example of interaction with the device

**STEP 1**

Prior to turning on the detector, the `successful measurement flag` and the `detector start flag` are zero, the status of the remaining registers is not defined and is arbitrary:

||**Control block**||||||
|-|-|-----------------|-|-|-|-|
|**offset**|0|1|2|3|4|5|
|**title**|Detector start flag|Mask flag|Temperature flag|Time parameter|Signal mode|Thermal image|
|**values**|**0**|-|-|-|-|-|
||**Result block**||||||
|**offset**|0|1|2|3|4||
|**title**|Successful measurement flag|Temperature measurement result||Result of determining the state of the mask|Temperature state result|
|**values**|**0**|-|-|-|-||

**STEP 2**

Slave changes the state of the `detector start flag` in the control block to value 1, thereby activating the detector on the master device. In addition, it sets the  `Mask flag` and `Temperature flag` flags the need to measure the temperature and the mask, and the `Time parameter` to detector’s operating time, and regular mode `Signal mode` indicating that we will open the turnstile if a mask and temperature is ok:

||**Control block**||||||
|-|-|-----------------|-|-|-|-|
|**offset**|0|1|2|3|4|5|
|**title**|Detector start flag|Mask flag|Temperature flag|Time parameter|Signal mode|Thermal image|
|**values**|**1**|**1**|**1**|**20**|**0**|**0**|
||**Result block**||||||
|**offset**|0|1|2|3|4||
|**title**|Successful measurement flag|Temperature measurement result||Result of determining the state of the mask|Temperature state result|
|**values**|0|-|-|-|-||

**STEP 3**

The Master device reads the registers by calling the function 0x03 (modbus_read_registers), Then changes the status of the `start detector flag` in the control block to 0, by calling function 0x06 (modbus_write_register) and starts the detector:

||**Control block**||||||
|-|-|-----------------|-|-|-|-|
|**offset**|0|1|2|3|4|5|
|**title**|Detector start flag|Mask flag|Temperature flag|Time parameter|Signal mode|Thermal image|
|**values**|**0**|1|1|20|0|0|
||**Result block**|||||
|**offset**|0|1|2|3|4|
|**title**|Successful measurement flag|Temperature measurement result||Result of determining the state of the mask|Temperature state result|
|**values**|0|-|-|-|-||

**STEP 4**

The master device fills in the result block with current intermediate values, calling the function 0x10 (modbus_write_registers).

This step can be performed many times, for example, if a person came without a mask and then put it on, or if his temperature changed while he was in the active area.
This step may also not be performed even once if there are no temperature measurement results and a mask. For example, if the person left before the end of the measurement.

||**Control block**||||||
|-|-|-----------------|-|-|-|-|
|**offset**|0|1|2|3|4|5|
|**title**|Detector start flag|Mask flag|Temperature flag|Time parameter|Signal mode|Thermal image|
|**values**|0|1|1|20|0|0|
||**Result block**||||||
|**offset**|0|1|2|3|4||
|**title**|Successful measurement flag|Temperature measurement result||Result of determining the state of the mask|Temperature state result|
|**values**|**5**|**0x6666**|**0x4200**|**1**|**1**||

**STEP 5**

The master device fills in the result block with current values, calling the function 0x10 (modbus_write_registers). The person found in the camera zone, he has the correct mask and normal temperature. Slave device can open the turnstile. If a person has an incorrectly worn mask or fever, this step will not be performed. If a person puts on a mask while in the camera area, this step will be performed after putting on the mask.

||**Control block**||||||
|-|-|-----------------|-|-|-|-|
|**offset**|0|1|2|3|4|5|
|**title**|Detector start flag|Mask flag|Temperature flag|Time parameter|Signal mode|Thermal image|
|**values**|0|1|1|20|0|0|
||**Result block**||||||
|**offset**|0|1|2|3|4||
|**title**|Successful measurement flag|Temperature measurement result||Result of determining the state of the mask|Temperature state result|
|**values**|**2**|**0x6666**|**0x4212**|**1**|**1**||

**STEP 6**

At the end of the measurement, the device fills in the result block with final values, calling the function 0x10 (modbus_write_registers), in this example the measurement result is 36.5, the mask is put on correctly:

||**Control block**||||||
|-|-|-----------------|-|-|-|-|
|**offset**|0|1|2|3|4|5|
|**title**|Detector start flag|Mask flag|Temperature flag|Time parameter|Signal mode|Thermal image|
|**values**|0|1|1|20|0|0|
||**Result block**||||||
|**offset**|0|1|2|3|4||
|**title**|Successful measurement flag|Temperature measurement result||Result of determining the state of the mask|Temperature state result|
|**values**|**1**|**0x6667**|**0x4214**|**1**|**1**||

* Also, note that the order of steps 4,5,6 is not guaranteed. For example, if at once the temperature and the mask were within the normal range, 5 and then 4 steps can be performed, with the same register values.

**STEP 7**

Slave sets the `successful measurement flag` to 0, indicating the receipt of the result, and returning the values of the registers to the initial state, similar to step 1:

||**Control block**||||||
|-|-|-----------------|-|-|-|-|
|**offset**|0|1|2|3|4|5|
|**title**|Detector start flag|Mask flag|Temperature flag|Time parameter|Signal mode|Thermal image|
|**values**|0|1|1|20|0|0|
||**Result block**||||||
|**offset**|0|1|2|3|4||
|**title**|Successful measurement flag|Temperature measurement result||Result of determining the state of the mask|Temperature state result||
|**values**|**0**|0x6666|0x4212|1|1||

---

## Stopping temperature measurement

During the execution of the temperature measurement and mask process (steps 4 and 5), the Slave device can stop the measurement.
To do this, `control register`, in the` detector start flag` the slave device must write the value `2`. Master device will stop measuring. In this case, the value of the `detector start flag` will be set by the Master to `0`.

After stopping the measurement, **step 6** will be performed, without waiting for the person to leave the camera area.

Also note that the `master device` **must write the value 0** to the` detector start flag` register before stopping the measurement. Because, we cannot guarantee that the device has already read the previous value, and triggered the detector.

If the detector is already started, but the response (value 0 in the `detector start flag`) has not yet been written, then the request to stop the detector will be ignored, the device will start measuring the temperature, and then write a value equal to 0 after the detector starts.

It is also worth noting that the device, before stopping the measurement, must have time to show the result on the screen and play a voice message.
When the measurement is stopped, the message on the screen will be cleared when processing the next camera frame. However, the voice message can be played if it has already started playing.

---

## Float conversion

The source code of the method, the conversion of float variable, to 2 register values, in the Java language used on the device:

```
    public static void setFloat(float f, int offset, char[] dest) throws ModbusException {
        if(offset+2> dest.length){
            throw new ModbusException("Outside offset array size!");
        }
        int i = Float.floatToIntBits(f);
        dest[offset] = (char) (i& 0xFFFF);
        dest[offset+1] = (char)((i >> 16)&0xFFFF);
    }
```
---
